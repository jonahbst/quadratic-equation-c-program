#include <stdio.h>
#include <math.h>

void findRoots(double a, double b, double c);

int main() {
    double a, b, c;

    printf(" Please Enter coefficients a, b, and c of the quadratic equation (ax^2 + bx + c = 0):\n");
    scanf("%lf %lf %lf", &a, &b, &c);

    findRoots(a, b, c);

    return 0;
}

void findRoots(double a, double b, double c) {
    double discriminant, root1, root2, realPart, imaginaryPart;

    // Calculating the discriminant
    discriminant = b * b - 4 * a * c;

    // checking type of root
    if (discriminant > 0) {
        // Real and distinct roots
        root1 = (-b + sqrt(discriminant)) / (2 * a);
        root2 = (-b - sqrt(discriminant)) / (2 * a);
        printf("Roots are real and distinct: %.2lf and %.2lf\n", root1, root2);
    } else if (discriminant == 0) {
        // Real and equal roots
        root1 = root2 = -b / (2 * a);
        printf("Roots are real and equal: %.2lf and %.2lf\n", root1, root2);
    } else {
        // Complex roots
        realPart = -b / (2 * a);
        imaginaryPart = sqrt(-discriminant) / (2 * a);
        printf("Roots are complex and conjugates: %.2lf + %.2lfi and %.2lf - %.2lfi\n", realPart, imaginaryPart, realPart, imaginaryPart);
    }
}
